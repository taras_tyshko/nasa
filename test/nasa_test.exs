defmodule NasaTest do
  use ExUnit.Case
  doctest Nasa

  test "Launch the ship from the Earth" do
    assert Nasa.calculation_fuel(28801, [{:launch, 9.807}]) ==
             [%{"fuel" => 19772, "gravity" => 9.807, "type" => :launch}]
  end

  test "land on the Moon" do
    assert Nasa.calculation_fuel(28801, [{:land, 1.62}]) ==
             [%{"fuel" => 1535, "gravity" => 1.62, "type" => :land}]
  end

  test "Launch on the Mars" do
    assert Nasa.calculation_fuel(28801, [{:launch, 3.711}]) ==
             [%{"fuel" => 5186, "gravity" => 3.711, "type" => :launch}]
  end
end
