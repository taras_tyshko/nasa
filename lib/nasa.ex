defmodule Nasa do
  @moduledoc """
  Documentation for `Nasa`.
  """
  @doc """
  Hello world.

  ## Examples

      iex> Nasa.calculation_fuel(28801, [{:land, 9.807}])
      [%{"fuel" => 13447, "gravity" => 9.807, "type" => :land}]


  """
  @spec calculation_fuel(integer(), tuple()) :: list(integer()) | list(tuple())
  def calculation_fuel(mass, args) do
    Enum.map(args, &fuel(mass, &1, 0))
  end

  @spec fuel(integer(), tuple(), integer()) :: integer()
  defp fuel(mass, {type, gravity}, fuel) when mass == 0 do
    %{"type" => type, "gravity" => gravity, "fuel" => fuel}
  end

  defp fuel(mass, {:land, gravity} = args, fuel) do
    result =
      (mass * gravity * 0.033 - 42)
      |> trunc()
      |> define_of_fuel_residue()

    fuel(result, args, fuel + result)
  end

  defp fuel(mass, {:launch, gravity} = args, fuel) do
    result =
      (mass * gravity * 0.042 - 33)
      |> trunc()
      |> define_of_fuel_residue()

    fuel(result, args, fuel + result)
  end

  @spec define_of_fuel_residue(integer()) :: integer()
  defp define_of_fuel_residue(value) do
    case value do
      value when value > 0 ->
        value

      _-> 0
    end
  end
end
