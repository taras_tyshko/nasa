# Elixirator Backend Task for Nasa

**The goal of this application is to calculate fuel to launch from one planet of the Solar system, and to land on another planet of the Solar system, depending on the flight route.**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `nasa` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:nasa, "~> 0.1.0"}
  ]
end
```
## Usage

- Download the project to the local machine.
- Go to the folder you downloaded
```
cd nasa
```
- Start an `iex` session inside the project by running
```
iex -S mix
```
- Call the function `calculation_fuel/2` in the module `Nasa`
- Parameters for function `Nasa.calculation_fuel/2`
    * weight
    * [{:type, gravity}] =>  type: `:land` or `:launch` and the gravity of the planet.

### Planet Gravity Data
* Earth - 9.807 m/s2​
* Moon - 1.62 m/s2​
* Mars - 3.711 m/s2​

## Example
**For example, for Apollo 11 Command and Service Module, with weight of 28801 kg, to land it on the Earth,
 required amount of fuel will be: `%{"fuel" => 13447}`**

```elixir
iex(1)> Nasa.calculation_fuel(28801, [{:land, 9.807}])
[%{"fuel" => 13447, "gravity" => 9.807, "type" => :land}]
```
 

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/nasa](https://hexdocs.pm/nasa).

